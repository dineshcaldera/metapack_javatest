package com.metapack;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * NOTE : improved solution of {@link AccumulatorImpl} <p>
 * 
 * Concrete class to implement parallel {@link Accumulator} 
 * 
 * @author dineshcaldera
 *
 */

public class AccumulatorThreadImpl implements Accumulator {

	volatile int runningTotal = 0;
	private static final int POOL_SIZE = 5;
	
	@Override
	public int accumulate(int... values) {
		int total = 0;
		ExecutorService executor = Executors.newFixedThreadPool(POOL_SIZE);
		List<Future<Integer>> futureList = new ArrayList<Future<Integer>>();

		Callable<Integer> worker = new AccumulatorCallable(values);
		Future<Integer> submit = executor.submit(worker);
		futureList.add(submit);

		for (Future<Integer> future : futureList) {
			try {
				total += (future.get());
			} catch (InterruptedException e) {
				total = 0;
			} catch (ExecutionException e) {
				total = 0;
			}
		}

		executor.shutdown();
		runningTotal += total;

		return total;
	}

	@Override
	public int getTotal() {
		return runningTotal;
	}

	@Override
	public void reset() {
		runningTotal = 0;

	}

}
