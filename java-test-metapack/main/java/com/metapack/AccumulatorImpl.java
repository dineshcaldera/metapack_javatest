package com.metapack;

/**
 * 
 * Concrete class to implement {@link Accumulator} 
 * 
 * @author dineshcaldera
 *
 */
public class AccumulatorImpl implements Accumulator {
	
	private volatile int runningTotal = 0;

	@Override
	public int accumulate(int... values) {
		int total = 0;
		for(int value : values){
			total +=value;
		}
		runningTotal += total;
		return total;
	}

	@Override
	public int getTotal() {
		return runningTotal;
	}

	@Override
	public void reset() {
		runningTotal = 0;
	}

}
