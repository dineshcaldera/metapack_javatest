package com.metapack;


import java.util.concurrent.Callable;

/**
 * 
 * Callable class to return thread total.
 * 
 * @author dineshcaldera
 *
 */
public class AccumulatorCallable implements Callable<Integer> {

	private int[] values;

	AccumulatorCallable(int... values)  {
		this.values = values;

	}


	@Override
	public Integer call() throws Exception {

		int total = 0;

		for(int value : this.values){
			total +=value;
		}

		return total;
	}

}
