package com.metapack;

import static org.junit.Assert.*;

import static org.hamcrest.core.Is.is;
import org.junit.Before;
import org.junit.Test;

public class AccumulatorTest {

	Accumulator accumulator;
	
	@Before
	public void setUp() throws Exception {
		accumulator = new AccumulatorImpl();
	}

	@Test
	public void shouldAccumulatedZero() {
		//Given
		
		//When
		int actual = accumulator.accumulate();
		//Then
		assertThat(actual, is(0));
	}

	@Test
	public void shouldAccumulated() {
		//Given
		
		//When
		int actual = accumulator.accumulate(1,2,3);
		//Then
		assertThat(actual, is(6));
	}
	
	@Test
	public void shouldSingleRunningTotal() {
		//Given
		accumulator.accumulate(1,2,3);
		//When
		int actual = accumulator.getTotal();
		//Then
		assertThat(actual, is(6));

	}

	@Test
	public void shouldResetRunningTotal() {
		//Given
		accumulator.accumulate(1,2,3);
		//When
		 accumulator.reset();
		 int actual = accumulator.getTotal();
		//Then
		 assertThat(actual, is(0));
	}
	
	@Test
	public void shouldMultipleRunningTotal() {
		//Given
		accumulator.accumulate(1,2,3);
		accumulator.accumulate(4);
		//When
		int actual = accumulator.getTotal();
		//Then
		assertThat(actual, is(10));

	}

	

}
