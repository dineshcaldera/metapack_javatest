package com.metapack;

import org.junit.Test;
import static org.junit.Assert.*;

import org.junit.Before;

import static org.hamcrest.core.Is.is;

public class AccumulatorThreadTest {

	Accumulator accumulator;

	@Before
	public void setup(){
		accumulator = new AccumulatorThreadImpl();

	}

	@Test
	public void shouldAccumulatorThreadZero() {

		//Given
		accumulator.accumulate();
		//When
		int actual = accumulator.getTotal();
		//Then
		assertThat(actual, is(0));
	}

	@Test
	public void shouldAccumulatorThreadRunningTotal() {

		//Given
		accumulator.accumulate(1,2,3);
		//When
		int actual = accumulator.getTotal();
		//Then
		assertThat(actual, is(6));
	}

	@Test
	public void shouldAccumulatorThreadReset() {

		//Given
		accumulator.accumulate(1,2,3);
		//When
		accumulator.reset();
		int actual = accumulator.getTotal();
		//Then
		assertThat(actual, is(0));
	}


	@Test
	public void shouldAccumulatorThreadRunningMultiTotal() {

		//Given
		accumulator.accumulate(1,2);
		accumulator.accumulate(3);
		accumulator.accumulate(4);
		//When
		int actual = accumulator.getTotal();
		//Then
		assertThat(actual, is(10));
	}

	@Test
	public void shouldAccumulatorThreadRunningMultiReset() {

		//Given
		accumulator.accumulate(1,2);
		accumulator.accumulate(3);
		accumulator.accumulate(4);
		//When
		accumulator.reset();
		int actual = accumulator.getTotal();
		//Then
		assertThat(actual, is(0));
	}

	@Test
	public void shouldAccumulatorThreadRunningMultiComplex() {

		//Given
		accumulator.accumulate(1,2);
		//When
		accumulator.accumulate(3);
		accumulator.reset();
		accumulator.accumulate(4);
		int actual = accumulator.getTotal();
		//Then
		assertThat(actual, is(4));
	}
}
